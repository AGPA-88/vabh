---
title: "Vagrant Plugins"
date: 2020-05-20T17:44:47+02:00
draft: false
author: "A.Alexandre" 
---
Hello everyone, I've created this post to explain what is a **Plugin** on Vagrant, how to install them and use them correctly in our vagrantfile;
This post will be updated as I'll get more information about other plugins

<!--more-->

To start lets take a look at a simple vagrantfile containing a Windows Server 2019 box:

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.box = "mcree/win2019"
  config.vm.box_version = "1.0.1584095692"

  config.vm.provider "virtualbox" do |v|
    v.name = "Windows Server 2019"
    v.memory = "2048"
  end

  config.vm.provision "shell", inline: <<-SHELL
    
    
  SHELL
```

This is just to give you a visual of a very simple vagrantfile containing just the box, the name of the vm, RAM memory and a shell terminal ready to go.

Let's proceed!


## Reload

What's the use of **reload** ?

Well, using reload in your vagrantfile is the same as doing a reboot in your machine;

So, lets put this into context;

Say that we've installed Chocolatey and want to use it to install git. 

Well that's easy, but we can't do it right away because since the vm was not reloaded (rebooted) it can't take into account the modification made previously;

So, to fix this, we need to add a reload provisioner to our vagrantfile! 

```
config.vm.provision :reload
```

The above command is what we need to add to our **vagrantfile** but we can't use it if it's not installed.

So,to install the **reload** porvisioner open a Powershell terminal and type:

```
vagrant plugin install vagrant-reload
```

This will install the plugin that will allow you to use the **reload** provisioner on our vagrantfile;

Do you remember our simple vagrantfile ? Lets take a look of how the vagrantfile looks with the installation of Chocolatey:

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.box = "mcree/win2019"
  config.vm.box_version = "1.0.1584095692"

  config.vm.provider "virtualbox" do |v|
    v.name = "Windows Server 2019"
    v.memory = "2048"
  end

  config.vm.provision "shell", inline: <<-SHELL
    
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

  SHELL
```

In our vagrantfile above, we install Chocolatey. But, remember, we want to use it to install our tools and without reloading the vm this won't be possible!

What we're going to do is, reload after installing Chocolatey and after reloading where going to open a new **Shell** terminal provisioner so we can install Git using Chocolatey;

So, lets take a look at our vagrantfile and see how all of this works put togheter:

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.box = "mcree/win2019"
  config.vm.box_version = "1.0.1584095692"

  config.vm.provider "virtualbox" do |v|
    v.name = "Windows Server 2019"
    v.memory = "2048"
  end

  config.vm.provision "shell", inline: <<-SHELL
    
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

  SHELL

  config.vm.provision :reload

  config.vm.provision "shell", inline: <<-SHELL
    
    choco install git -y

  SHELL
```

Great! We achieved the installation of the plugin and the successful reload of our vm !


## Disk Size

Another usefull plugin is the **Disk Size**!

This plugin will allow us to manage the disk size of our vm (everybody who uses Vagrant knows that this is a sensible issue :) )

As seen previous, we first need to install the plugin directly to our machine so we can use it in our vagrantfile.

So, copy paste the following:

```
vagrant plugin install vagrant-disksize
```

Great, the plugin is installed and ready to be put to work!

Lets see how it looks:

```
config.disksize.size = '40GB'
```

As you can see, i want have a 40gb disk size. but where should we place it? Lets have a look at a basic vagrantfile:

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.box = "mcree/win2019"
  config.vm.box_version = "1.0.1584095692"

  config.vm.provider "virtualbox" do |v|
    v.name = "Windows Server 2019"
    v.memory = "2048"
  end

```

Very simple and basic vagrantfile but there's something missing! Where's the **disk size**?

Well, since it's a part of the configuration of the vm we will place it just after the vm box:

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.box = "mcree/win2019"
  config.vm.box_version = "1.0.1584095692"
  wvm.disksize.size = "40GB"

  config.vm.provider "virtualbox" do |v|
    v.name = "Windows Server 2019"
    v.memory = "2048"
  end

```

And there it is! You can now create vm's with the amount of disk size that you wish :)

**NOTE:** If you start your vm and see that your windows vm doesn't have the disk size you attributed and probably only have up to 20gb of disk size. 

The other amount is not attributed to the right partition.
To correct this we'll type the following command into a Powershell: 
(don't forget at the end of the command line to add your GB, in my case I gave 40gb so i'll put 39 at the end of the command)

```
Resize-Partition -DriveLetter C -Size 39gb
```


Hope to see you soon on the next post :)

