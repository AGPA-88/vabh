---
title: "How to deploy an Ubuntu virtual machine"
date: 2020-04-27T17:18:01+02:00
draft: false
---
# This post will explain to you how to deploy an Ubuntu machine very easily and quick!
## What do you need?
We will use 2 tools to make the deployment of the Ubuntu vm very fast! 

These tools are Vagrant and Virtual Box.

## Installation

### By Download
Note that you have to choose the OS where you wish to install the tool (Linux, Windows, etc...)

- Vagrant -> https://www.vagrantup.com/downloads.html

- Virtual Box -> https://www.virtualbox.org/wiki/Downloads

### By using package management

#### Windows - Chocolatey

> To start you need to install Chocolatey. For that I have a post explaning how to it correctly here ->
> Now that you have installed Chocolatey lets use it.
> Lets start with VirtualBox, so for this use the following command:
```
choco install virtualbox
```
> And now Vagrant:
```
choco install vagrant
``` 

#### Linux - apt

> This is the same concept as **choco install** but for Linux of course.
> So, for Linux, we will use **apt install**.
> For VirtualBox:
```
sudo apt install virtualbox
```
> For Vagrant:
```
sudo apt install vagrant
```

## Deployment:
So, first we need the Vagrant box (don't forget to see the Vagrant post on the page to learn a bit more about Vagrant boxes). For this example we will use Ubuntu 18.04 LTS.
https://app.vagrantup.com/ubuntu/boxes/bionic64

Now, in your local machine, access your terminal, create a folder for your Vagrant box and, inside your new folder, type:

```
vagrant init ubuntu/bionic64
```

Doing this will add the box to your local Vagrant. 
And now, let's create our Ubuntu virtual machine! So tap in your terminal:

```
vagrant up
```

And there it is! Your first fresh and clean Ubuntu vm!
