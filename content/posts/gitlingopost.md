---
title: "Get to know GIT actions and lingo"
date: 2020-05-27T20:19:53+02:00
draft: false
author: "Lucas Terquem"
translator: "antonio" 
---
Before I start this post just want to make clear that this post was not created by me.
This post is a traduction from a [Lucas Terquem](https://www.padok.fr/blog/author/lucas-terquem) article that he did and posted over [here](https://www.padok.fr/blog/commit-mep-developpeur).

I like Lucas article because it explains, in a very simple and easy way, some commom actions and language used in a IT environment almost daily.

<!--more-->

# Git, commit, repo ... Understand the daily actions of a developer in five minutes


The daily gestures of developers are full of technical terms that make them obscure for profiles that are more into the business side.

However, to be able to understand the challenges of developers, it is crucial to understand how they work, and to be able to dialogue with them using a common vocabulary.

For that, we'll be explaining in this article some terms and associated practices to allow you to better communicate within your team.

## Summary

1. The versioning tool 
2. Local development 
3. Adding code to the shared repo 
4. Putting into production (MEP)

The versioning tool

When developers are working on a project, they use a **version control tool**, or **versioning**. This type of tool manages file versions, and therefore allows you to avoid this:

(screenshot)

This tool will **allow all developers to work together on the same project**, while keeping **track of the modifications made** to the files.

The two best known tools are **GitLab** and **GitHub**, both of which are based on the Git version control system, and provide many useful features for developers.

A similar tool is used on Google sheets for example: 

(screenshot)

Here we will simplify the process but keep the technical terms, which will allow you to communicate with techs in a **common language**.

## Local development

A developer works on a **“local” environment**, which is installed on his computer to simulate the product. He has a **copy of the product code on his computer**, and makes changes to it. This copy is in a folder monitored by versioning software, called **repo** (for repository). This repo on the developer's computer is the **local repo**.

For the example, we are going to take a developer who wants to add a button on his site, which brings up a cookie (the cake, not the tracker) on the user's screen. This is called a **feature**.

To do this, he will **modify the copy of the site code stored on his computer**, to add a button to the site, create the function that will cause a cookie to appear, and activate the function when the button is clicked.

(screenshot)

When he is satisfied with his modifications and **has tested them on his local environment**, the developer will report these modifications to the versioning software, via an add (the command used is "git add").


## Adding code to shared repo

It will then ask the versioning software to **update the copy of the code that is in the local repo**, to add these updates, by making a **commit** (using the command “git commit”, you understand the logic) .

(screenshot)

So we have a developer who has a **local repo which contains the site code**, plus its function which causes a cookie to appear. Our developer will now want to integrate his modification into the product code. For that, he will have to **update the shared repo** (we hadn't talked about that yet but don't panic) which is stored on an online platform, generally GitLab or GitHub, and which contains the reference code of the project, on which all the developers are collaborating.

In summary, each developer works on a local copy of the project code, then **integrates his modifications into the shared code**. To integrate these modifications, he will push (yes, we suspect it, thanks to the "git push" command) to update the shared repo.

However, it will not directly update the source code of the project: it might introduce bugs. It will go through a **branch**, which is an **alternative version of the code**. On a project, you can for example have the **“master” branch** (which is conventionally the source code put into production), and the “dev” branch used by developers to apply their modifications.


## Production start-up (MEP)

Having several branches allows developers to integrate all their modifications on the “dev” branch, **to test the “dev” code on a special environment** (let's be crazy, let's call it “dev env”) , and, once you are satisfied with the code of the dev branch, **update the master code with the modifications made**. This process is a **production start-up** (or MEP), and is a **crucial stage for a company**: it is the moment when the work of developers becomes accessible to the user (the client of product, for example).

**Side Note:** a production can take place once every few months, for companies generally having a low level of DevOps integration, several times a day or automatically at the request of the developers, for companies having invested in a DevOps approach and an efficient CI / CD process.

Our developer will therefore add his modification to a dedicated branch. The convention depends on the projects, and is generally to have **one branch per feature** (“dev-BoutonQuiFaitPopDesCookiesSonLEcranDeMonUutilateurFourLuiDonnerHaim” <- it's a bit long, but you get the idea), and, in case several developers are working on the same feature, to have **one sub-branch per developer** (“dev-BoutonQuiFaitPopDesCookiesSonLEcranDeMonUutilateur PourLuiDonnerFaim-Paul”).

It will then request the integration of this branch into the code of the shared branch (the “dev” branch for example), by means of a functionality of the code management platform used, the **merge request** (on GitLab, called **pull request** on GitHub). This requires additional validation to activate the update of the branch, and allows to set up a **code review** process, that is to say validation by another developer of the added code, which allows to **limit the errors**.

(screenshot)

Once the **merge request** (or **MR**, **PR** for pull request) is validated, the modification is applied and the feature added to the code of the “dev” branch! The code of this branch will then be **deployed on a test environment** (for example hosted on www.dev.monsite.fr, and accessible only to developers). When the developers have repeated this process, added a certain number of functionalities and tested on the dev app that the code works well, they will **be able to put their code into production by doing an MR from dev to master, and deploy master on the env of prod** (which is the environment used by users, hosted for example on www.monsite.fr)

(screenshot)


## Conclusion

This process is iterative and asynchronous: developers are constantly working on new features through these stages, from the **creation of the feature in their local environment to its integration in the dev branch** (also called “staging” or “pre- prod ”depending on the project). Using a **versioning platform** allows a large number of developers to work simultaneously, alone or with others on a feature.

The frequency of production starts depends on a lot of security, testing, operational prerequisites, etc. and therefore varies enormously depending on the company.

 

Congratulations, you can now speak like a tech! Here are some sentences (which you should now understand) that you can use:

   > No wonder you end up with bugs on this feature if no one has reviewed the RM

   > It's normal that we don't have this form on dev, [X] left yesterday without pushing his code
  
   > We have a planned MEP this afternoon, did you make your commits well?


