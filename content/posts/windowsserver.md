---
title: "Create a Windows Server VM with Vagrant"
date: 2020-05-05T11:21:08+02:00
draft: false
---
Hello everyone, I've created this post to explain how we can create a Windows Server VM using Vagrant with some extra applications. We will learn how to install all of the tools that we need to make our server VM.
We will need Vagrant, Chocolatey, Git, Hugo and VSCode.

<!--more-->

Let's start!


## Vagrant:

Now lets see how we can create one to our needs and wants.

So, first things first, the VM. We decided to go with a Windows Server 2019 os and to do so we searched for a Windows Server box on [vagrants box websit](https://app.vagrantup.com/boxes/search)
In the website just tap, into the search box, the type of vm your looking for (could be Linux, Windows, etc). In our case we just have to type "mcree/win2019" and you'll find it.
Inside the box page you'll find "Vagrantfile" and "New". Our interest for this post is the "Vagrantfile".
As you can see in the box page there's the initial Vagrantfile configuration. You will need this to create (up) your virtual server machine.
This is the start of our Vagrantfile:

```
Vagrant.configure("2") do |config|
  config.vm.box = "mcree/win2019"
  config.vm.box_version = "1.0.1584095692"
end
```
As you can see, we get the box for our vm and its version.

Now, lets add the name of our vm and the amount of memory (RAM) do we want to give to it:

```
wvm.vm.provider "virtualbox" do |v|
  v.name = "Windows Server 2019"
  v.memory = "2048"
end
```
Now that we have our vm name and RAM capacity lets configure the network:

```
wvm.vm.define "Windows Server 2019" do |web|
  web.vm.hostname = "winserver19"
  web.vm.network "public_network"
end
```
Here we define the hostname of the vm and the type of network that we wish to give the vm (Public, Private, etc)

Note that there's multiple variants of network configuration, this post will only show you the most basic way to **Up** your vm;

And the Vagrantfile should look something like this by now:

```
Vagrant.configure("2") do |config|
  config.vm.box = "mcree/win2019"
  config.vm.box_version = "1.0.1584095692"

  config.vm.provider "virtualbox" do |v|
    v.name = "Windows Server 2019"
    v.memory = "2048"
  end

  wvm.vm.define "Windows Server 2019" do |web|
    web.vm.hostname = "winserver19"
    web.vm.network "public_network"
  end
end
```

This is a very simple and easy (just how we like it) to configurate Vagrantfile.

But we want to have some tools installed on the vm when we **Up** our virtual machine. To do so we're going to use a terminal (shell).

So, we need to add to the Vagrantfile the terminal:

```
config.vm.provision "shell", inline: <<-SHELL
  
SHELL
```
This is the terminal that is going allow you to give commands to add, remove, move and a bunch of other things to the vm;

So, we're going to install Chocolatey (see below explaination of what is chocolatey and how to install it) and, using Chocolatey, we're going to install Git, VSCode and Hugo;

Note that **SHELL** at the end of the terminal replaces the **end** in the other configurations seen previously.
 

## Chocolatey:

Chocolatey is a module for windows to install packages (tools) on to the machine. For example, "choco install tool" is the equivalent to "apt install tool" on Ubuntu.
So to install it you need to copy paste this command to your vagrantfile shell provisioner:

```
config.vm.provision "shell", inline: <<-SHELL
  
  Set-ExecutionPolicy Bypass -Scope Process -orce; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
  
SHELL
```

This command will install chocolatey and allow you to use "choco install" to install other tools into your windows server such as Git or VSCode.

## Tools:

Now you need to install the tools that you need and to do so you'll use Chocolatey!
So, add to your vagranfile shell the following commands:

```
choco install git -y
choco install vscode -y
choco install hugo -y
```

These commands will install the respective tools, but you can install the tools that you need. The only thing that you need to is **choco install "tool that you want"** :)
Note, that the **-y** is to answer Yes to the questions asked by the installation process;

So, our Vagrantfile looks like it's complete! So lets see how it looks like with all put togheter:

# Vagrantfile

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.box = "mcree/win2019"
  config.vm.box_version = "1.0.1584095692"

  config.vm.provider "virtualbox" do |v|
    v.name = "Windows Server 2019"
    v.memory = "2048"
  end

  wvm.vm.define "Windows Server 2019" do |web|
    web.vm.hostname = "winserver19"
    web.vm.network "public_network"
  end

  config.vm.provision "shell", inline: <<-SHELL
  
    Set-ExecutionPolicy Bypass -Scope Process -orce; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    choco install git -y
    choco install vscode -y
    choco install hugo -y

  SHELL

end
```


