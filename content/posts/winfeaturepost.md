---
title: "How to Manage Windows Features with Ansible"
date: 2020-05-22T11:39:11+02:00
draft: false
author: "A.Alexandre" 
---

Hello everyone, the goal of this post is to explain how to manage Windows Features using Ansible playbooks;

<!--more-->

To start, lets try and understand what is what. We want to install a windows feature in our machine/vm and we want to do so by using [Ansible](https://agpa-88.gitlab.io/vabh/posts/ansiblepost/) to automate all the installations and add-ons.

To do so using Ansible we need to use Ansible modules! And, in this case, we want to use **win_feature** module. But what are modules? Lets see:

## Modules

Ansible modules are reusable, standalone scripts that can be used by the Ansible API, or by the ansible or ansible-playbook programs.

We can use modules to install, copy, move, download and a lot of others actions. There's also a lot of different modules, and I mean a lot!

Ansible has modules for Cloud, Database, Network, Monitoring, Windows, almost everything you can imagine!

But for this post we'll be focusing just on a specific windows module provided by Ansible that is **win_feature** module.

**NOTE** that this is just a quick overview of what modules are! You can check the official Ansible documetation on [Modules](https://docs.ansible.com/ansible/latest/modules/modules_by_category.html) to have a more in depth analyses.

## win_feature

This module installs or uninstalls Windows Roles or Features on Windows Server.

But, to know which feature we want to install we need to find its name. And for that we'll use a command that will list all of the features with the added information if they're installaed or not.

So type in your terminal the following:

```
Get-WindowsFeature
```
This should return a full list of the features:

```
PS C:\> Get-WindowsFeature

Display Name                                            Name                       Install State
------------                                            ----                       -------------
[ ] Active Directory Certificate Services               AD-Certificate                 Available
    [ ] Certification Authority                         ADCS-Cert-Authority            Available
    [ ] Certificate Enrollment Policy Web Service       ADCS-Enroll-Web-Pol            Available
    [ ] Certificate Enrollment Web Service              ADCS-Enroll-Web-Svc            Available
    [ ] Certification Authority Web Enrollment          ADCS-Web-Enrollment            Available
    [ ] Network Device Enrollment Service               ADCS-Device-Enrollment         Available
    [ ] Online Responder                                ADCS-Online-Cert               Available

(...)

[X] Web Server (IIS)                                    Web-Server                     Installed
    [X] Web Server                                      Web-WebServer                  Installed
        [X] Common HTTP Features                        Web-Common-Http                Installed
            [X] Default Document                        Web-Default-Doc                Installed
            [X] Directory Browsing                      Web-Dir-Browsing               Installed
            [X] HTTP Errors                             Web-Http-Errors                Installed
            [X] Static Content                          Web-Static-Content             Installed
            [X] HTTP Redirection                        Web-Http-Redirect              Installed
            [X] WebDAV Publishing                       Web-DAV-Publishing             Installed
        [X] Health and Diagnostics                      Web-Health                     Installed
            [X] HTTP Logging                            Web-Http-Logging               Installed
            [X] Custom Logging                          Web-Custom-Logging             Installed
            [X] Logging Tools                           Web-Log-Libraries              Installed
            [X] ODBC Logging                            Web-ODBC-Logging               Installed
            [X] Request Monitor                         Web-Request-Monitor            Installed
            [X] Tracing                                 Web-Http-Tracing               Installed
        [X] Performance                                 Web-Performance                Installed
            [X] Static Content Compression              Web-Stat-Compression           Installed
            [X] Dynamic Content Compression             Web-Dyn-Compression            Installed


(...)

[X] .NET Framework 4.7 Features                         NET-Framework-45-Fea...        Installed
    [X] .NET Framework 4.7                              NET-Framework-45-Core          Installed
    [ ] ASP.NET 4.7                                     NET-Framework-45-ASPNET        Available
    [X] WCF Services                                    NET-WCF-Services45             Installed
        [ ] HTTP Activation                             NET-WCF-HTTP-Activat...        Available
        [ ] Message Queuing (MSMQ) Activation           NET-WCF-MSMQ-Activat...        Available
        [ ] Named Pipe Activation                       NET-WCF-Pipe-Activat...        Available
        [ ] TCP Activation                              NET-WCF-TCP-Activati...        Available
        [X] TCP Port Sharing                            NET-WCF-TCP-PortShar...        Installed

(...)

```

Now we can see a full list of all Windows features (in the example above there's only a selected few) with the added information if they're installed or not.

This could be a great way to see what features do you need to add to your playbook.

Now, let me show you a way to find out if a a feature that you already know the name is installed or not. 
 
For this case we'll search for the .NET Framework feature. We're going to check the feature correct name and if it's installed or not.
 
For that we'll use the command below:

```
Get-WindowsFeature | Where-Object {$_. name-match ".Net"} | Format-List Name,DisplayName, Installstate | more
```
This will return the following:

```
Name         : Print-Internet
DisplayName  : Internet Printing
InstallState : Available

Name         : Web-Net-Ext
DisplayName  : .NET Extensibility 3.5
InstallState : Installed

Name         : Web-Net-Ext45
DisplayName  : .NET Extensibility 4.7
InstallState : Installed

Name         : Web-Asp-Net
DisplayName  : ASP.NET 3.5
InstallState : Installed

Name         : Web-Asp-Net45
DisplayName  : ASP.NET 4.7
InstallState : Installed

Name         : NET-Framework-45-ASPNET
DisplayName  : ASP.NET 4.7                             #As you can see the feature is available, therefore not installed
InstallState : Available

Name         : BitLocker-NetworkUnlock
DisplayName  : BitLocker Network Unlock
InstallState : Available

Name         : Internet-Print-Client
DisplayName  : Internet Printing Client
InstallState : Available

Name         : Telnet-Client
DisplayName  : Telnet Client
InstallState : Available

Name         : WAS-NET-Environment
DisplayName  : .NET Environment 3.5
InstallState : Available

Name         : Wireless-Networking
DisplayName  : Wireless LAN Service
InstallState : Available
```

Now that we confirmed the name and the install state we can add to our playbook.yaml the win_feature module that we talked about earlier in the post.

Lets take a look at playbook.yaml containing the **win_feature** module

```
---
- name: Install Windows Features
  hosts: all
  tasks:
          - name: Install .Net Framework 4.5
            win_feature:
                    name: NET-Framework-45-ASPNET
                    state: present
``` 

As you can see, playbook.yaml will install the latest .Net Framework Windows feature.

Lets search for the .net Framework feature on our terminal and see what changed:

```
PS C:\> Get-WindowsFeature | Where-Object {$_. name-match "NET-Framework-45-ASPNET"} | Format-List Name,DisplayName, Installstate | more


Name         : NET-Framework-45-ASPNET
DisplayName  : ASP.NET 4.7
InstallState : Installed

```

And there it is! We successfully installed a windows feature of our choosing using Ansible playbooks!

Now, is up to you to see what Windows Feature do you wish to install on your server!

Remember that you can make a playbook installing as many features as you wish.
I hope that this post will help you understand a bit better the win_feature module :)

